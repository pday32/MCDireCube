MCDireCube
===============

MCDireCube is a **ClassiCube Server Software** based on MCGalaxy which itself is based on MCForge, which is in turn based on MCLawl.

Currently it is being used as the software that powers the official DireCube server.

Pull requests are welcomed, but will be merged in at our own discretion.

Download the latest MCGalaxy release [from here](https://github.com/TheCleverDire/MCDireCube/releases)
Copyright/License
-----------------
See LICENSE for MCGalaxy license, and license.txt for code used from other software.
